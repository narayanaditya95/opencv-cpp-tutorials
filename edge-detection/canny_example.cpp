#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

void UseCanny(Mat img){

	Mat grayscale(img.rows,img.cols,CV_8UC1);
	cvtColor(img,grayscale,CV_BGR2GRAY);

	int lowthresh = 20, highthresh = 100;
	string win_name = "Edge-extracted image";

	namedWindow(win_name,CV_WINDOW_AUTOSIZE);
	createTrackbar("Low Threshold",win_name,&lowthresh,150);
	createTrackbar("High Threshold",win_name,&highthresh,255);
	

	while(1){

		Mat result = grayscale.clone();
		Canny(result,result,lowthresh,highthresh,3);
		imshow(win_name,result);
		char ch = waitKey(33);
		if(ch==27)
			break;
	}

	grayscale.release();
	destroyAllWindows();
}

int main(){

	string fname;
	cout<<"Enter name of file:";
	cin>>fname;

	Mat image = imread(fname);
	UseCanny(image);
	image.release();
	return 0;
}
