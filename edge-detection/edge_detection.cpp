#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

Mat EdgeDetect(Mat img, int threshold){

	Mat result(img.rows,img.cols,CV_8UC1);
	Mat grayscale(img.rows,img.cols,CV_8UC1);
	cvtColor(img,grayscale,CV_BGR2GRAY);

	int i,j,k,l,max,min,val,r = img.rows,c = img.cols;

	for(i=1;i<r-1;i++){
		for(j=1;j<c-1;j++){

			max = min = grayscale.at<uchar>(i,j);
			for(k=i-1;k<=i+1;k++){
				for(l=j-1;l<=j+1;l++){

					val = grayscale.at<uchar>(k,l);
					if(val>max)
						max = val;
					else if(val<min)
						min = val;
				}
			}

			if(max-min>threshold)
				result.at<uchar>(i,j) = 0;
			else
				result.at<uchar>(i,j) = 255;
		}
	}

	grayscale.release();
	return result;
}


int main(){

	string file_name;
	int threshold;
	cout<<"Enter name of file:";
	cin>>file_name;
	cout<<"Enter threshold:";
	cin>>threshold;

	Mat image = imread(file_name);
	Mat edge_img = EdgeDetect(image,threshold);
	imshow("Edges Extracted",edge_img);
	waitKey(0);
	image.release();
	edge_img.release();
	return 0;
}